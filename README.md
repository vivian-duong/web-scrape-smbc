# web-scrape-smbc

I wrote Python script that will download all [Saturday Morning Breakfast Comics](www.smbc-comics.com) published online and saves it in a directory on my desktop. 

I scraped the web using Python packages requests, os, and Beautiful Soup.

If I were to lose all my comic images, I would just open downloadSMBC.py and specify where to save the comics 
`folder_to_store_comics = 'path'` and then run the Python script.