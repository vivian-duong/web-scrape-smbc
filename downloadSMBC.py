#downloadSMBC.py - Downloads every single smbc comic

import requests, os, bs4, sys

def init_directory():
    is_dir_given = len(sys.argv) > 1
    if is_dir_given:
        dir_ = sys.argv[1]
    else:
        dir_ = 'smbc'
    
    os.makedirs(dir_, exist_ok=True)

def get_soup(url):
    res = requests.get(url)
    res.raise_for_status()
    soup = bs4.BeautifulSoup(res.text, "html.parser")
    return soup

def download_img(img_url):
    #download the comic img file
    res = requests.get(comic_url)
    res.raise_for_status()

    #save the img to Desktop\smbc
    img_file = open(os.path.join(dir_, os.path.basename(comic_url)), 'wb')
    for chunk in res.iter_content(10^5):
        img_file.write(chunk)
    img_file.close()

    
def click_prev(previous_buttons):
    if len(previous_buttons) != 0:
        target_prev_button = previous_buttons[0]
        url = target_prev_button.get('href')
    else:
        url = ''
    return url
    
    
init_directory()

domain = 'https://www.smbc-comics.com/'    
url = domain

while url != '':
    soup = get_soup(url)

    #css selector .className #id
    date_ = soup.select('.cc-publishtime')[0].getText()
    title = soup.select('#cc-comic')[0].get('src')
    comic_url = 'https://www.smbc-comics.com/' + title
    
    print('Downloading comic on %s' %url); print(date_, '\n')
    download_img(comic_url)

    prev_buttons = soup.select('.cc-prev')

    url = click_prev(prev_buttons)
    
print('Done.')